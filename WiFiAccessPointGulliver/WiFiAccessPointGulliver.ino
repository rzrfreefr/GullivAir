/*
 * Copyleft - VinCiv pour Gulliver - 2017
 * 
 * Code placé sous licence Gnu GPL3
 */

/* Create a WiFi access point and provide a web server on it. */

#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>

/* Set these to your desired credentials. */
const char *codeWiFi = "GullivAir";
const char *mdp = "gulliver";

ESP8266WebServer server(80);

char gMessage[256];

/* 
 * Pour accéder au serveur, taper l'adresse http://192.168.4.1
 */
void handleRoot() {
	static int count=0;
	StaticJsonBuffer<200> jsonBuffer;
	JsonObject& root = jsonBuffer.createObject();
	root["message"] = gMessage;
	root["count"] = ++count;
	root["latitude"] = 52.165000; //TODO
	root["longitude"] = -2.210000;
	root["temperature"] = 1; 
	root["NO2"] = 2;
	root["PM"] = 3;

	size_t len = root.measureLength();
	size_t size = len+1;
	char json[size];
	root.printTo(json, sizeof(json));
	server.setContentLength (size);
	server.send(200, "application/json", json);
}

void setup() {
	delay(1000);
	Serial.begin(115200);
 
	WiFi.softAP(codeWiFi, mdp);

	server.on("/", handleRoot);
	server.begin();
}

void loop() {
	int length = 256;
	char buffer[256];
	char character ='\n';
	length = Serial.readBytesUntil(character, buffer, length);
	if ( length > 0) {
		memcpy( gMessage, buffer, length);
		gMessage[length] = 0;
	}
	server.handleClient();
	delay(500);
}
