Ambassad'Air
===

## Requirements

### Arduino

 - [ArduinoJson](https://github.com/bblanchon/ArduinoJson)
 - [ESP8266](https://github.com/esp8266/Arduino/)

## Help

[ESP8266 WiFi examples](https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi/examples)

## Developement

Use [inliner](https://github.com/remy/inliner/) to minify/compile the html, javascript and css into a single file (index-min.html).

Inliner Usage: `inliner index.html > index-min.html`
