// ESP-Server is a hotspot which serves a simple html webpage to see the sensors data in real time
// Todo:
// - save the html to serve on a file on the ESP memory
// - enable the client to send the sensor data to an open database

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>
#include "FS.h"

const char *ssid = "AmbassadAir";
const char *password = "gulliver";

ESP8266WebServer server(80);

// Server main html file for the client interface (accessed from a smartphone for example)
// Todo: save the .html file in memory instead of hardcoding it?
void handleRoot() {
  // File f = SPIFFS.open("/index-min.html", "r");
	// size_t size = f.size();
	// char buffer[size];
	// f.readBytes(buffer, size);
	// server.send(200, "text/html", buffer);
	// server.send(200, "text/html", f.readString());
	server.send(200, "text/html", "<!DOCTYPE html> <html> <head> <style>body{ }</style> <script>var xmlhttp = new XMLHttpRequest(); var url = 'data/'; xmlhttp.onreadystatechange = function() { if (this.readyState == 4 && this.status == 200) { var data = JSON.parse(this.responseText); displayData(data); } }; function displayData(data) { document.getElementById('Temperature', data.Temperature) document.getElementById('N02', data.N02) document.getElementById('PM', data.PM) } getData() { xmlhttp.open('GET', url, true); xmlhttp.send(); } document.addEventListener('DOMContentLoaded', function(event) { setInterval(getData, 5000) });</script> </head> <body> <ul> <li>N20: <span id'n02'=''>--</span></li> <li>Particules: <span id'pm'=''>--</span></li> <li>Temperature: <span id'temperature'=''>--</span></li> </ul> </body> </html>");
}

// response to sensor data request (asked by the client javascript)
void handleDataRequest() {

	// Create a JSON object form the data
	StaticJsonBuffer<200> jsonBuffer;
	JsonObject& root = jsonBuffer.createObject();
	root["Temperature"] = 1;
	root["N02"] = 2;
	root["PM"] = 3;
	size_t len = root.measureLength();
	size_t size = len+1;
	char json[size];
	root.printTo(json, sizeof(json));

	server.setContentLength (size);
	// server.sendHeader (const String &name, const String &value, bool first=false)
	// server.sendContent (json);
	server.send(200, "application/json", json);
}

// Start wifi hotshopt
void setup() {

	delay(1000);
	Serial.begin(115200);
	Serial.println();
	Serial.print("Configuring access point...");

	WiFi.softAP(ssid, password);

	IPAddress myIP = WiFi.softAPIP();
	Serial.print("AP IP address: ");
	Serial.println(myIP);
	server.on("/", handleRoot);
	server.on("/data", handleDataRequest);
	server.begin();
	Serial.println("HTTP server started");
}

void loop() {
	server.handleClient();
}
