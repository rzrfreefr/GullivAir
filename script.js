
var xmlhttp = new XMLHttpRequest();
var url = "data/";

xmlhttp.onreadystatechange = function() {
	if (this.readyState == 4 && this.status == 200) {
		var data = JSON.parse(this.responseText);
		displayData(data);
	}
};

function displayData(data) {
	document.getElementById('Temperature', data.Temperature)
	document.getElementById('N02', data.N02)
	document.getElementById('PM', data.PM)
}

getData() {
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}

document.addEventListener("DOMContentLoaded", function(event) {
	setInterval(getData, 5000)
});
